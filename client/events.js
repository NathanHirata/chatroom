'use strict';
Template.form.events({

    "submit .add-new-post": function (event) {
        event.preventDefault();

        var postName = event.currentTarget.children[0].value;
        Collections.Posts.insert({
            name: postName,
            createdAt: new Date(),
            complete: false,
            username: Meteor.user().username
        });

        return false;
    }
});